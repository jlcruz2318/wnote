use crate::notes::{Note, Notebook};
mod notes;

fn main() {
    let x = Note::new();
    let mut new_notebook = Notebook::new();

    new_notebook.notes.push(x);

    println!("{:?}", new_notebook);

}