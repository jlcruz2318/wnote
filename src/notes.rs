use chrono::prelude::*;
use serde::{Serialize, Deserialize};
use uuid::Uuid;


#[derive(Serialize, Deserialize, Debug)]
pub struct Note {
    uuid: Uuid,
    pub title: String, 
    content: String,
    create_date: NaiveDateTime,
    last_modified_date: NaiveDateTime,
}

impl Note {
    pub fn new() -> Note {
        Note{
            uuid: Uuid::new_v4(),
            title: "New Note".to_string(), 
            content: "New Content".to_string(),
            create_date: Utc::now().naive_utc(),
            last_modified_date: Utc::now().naive_utc()
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Notebook {
    name: String,
    pub notes: Vec<Note>, 
}

impl Notebook {
    pub fn new() -> Notebook{
        Notebook {
            name: "New Notebook".to_string(),
            notes: Vec::new(),
        }
    }
}